﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSingleton : MonoBehaviour
{
    [SerializeField] float time;
    [SerializeField] private AudioClip clip;
    [SerializeField] public bool canPlay;

    void Awake()
    {
        var count = FindObjectsOfType<AudioSingleton>().Length;
        if (count > 1)
        {
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        } 
    }

    public IEnumerator PlayCutSound()
    {
        yield return new WaitForSeconds(time);
        AudioSource.PlayClipAtPoint(clip, new Vector3(0,0), 1);
    }
}
