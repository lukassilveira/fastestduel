﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] public Button[] inputButtons;
    [SerializeField] public GameObject[] target;
    [Space]
    [SerializeField] public int inputValue;
    [SerializeField] public int targetIndex = 0;
    [Space]
    [SerializeField] public Animator inputParent;
    [SerializeField] public Animator targetParent;
    [SerializeField] private GameMaster gameMaster;
    [SerializeField] public AudioSingleton soundObject;

    void Start()
    {
        RandomInputButtons();
        RandomTargetNumbers();
        soundObject = FindObjectOfType<AudioSingleton>();
    }

    public void RandomInputButtons()
    {
        int i = 0;
        List<int> existingNumbers = new List<int>();
        while (i < inputButtons.Length)
        {
            int randomIndex = Random.Range(1, 4);
            if (!existingNumbers.Contains(randomIndex))
            {
                inputButtons[i].GetComponentInChildren<Text>().text = randomIndex.ToString();
                existingNumbers.Add(randomIndex);
                i++;
            }
        }
    }

    public void RandomTargetNumbers()
    {
        int i = 0;
        List<int> existingNumbers = new List<int>();
        while (i < target.Length)
        {
            int randomIndex = Random.Range(1, 4);
            if (!existingNumbers.Contains(randomIndex))
            {
                target[i].GetComponentInChildren<Text>().text = randomIndex.ToString();
                target[i].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                target[i].GetComponentInChildren<Text>().color = new Color(1, 1, 0, 1);

                existingNumbers.Add(randomIndex);
                i++;
            }

            if (existingNumbers.Count > 2)
            {
                existingNumbers.Clear();
            }
        }
    }

    public void ComputeInput(GameObject button)
    {
        inputValue = int.Parse(button.GetComponentInChildren<Text>().text);
        if (inputValue == int.Parse(target[targetIndex].GetComponentInChildren<Text>().text))
        {
            Debug.Log("Right");
            target[targetIndex].GetComponent<Image>().color = new Color(1, 1, 0, 0);
            target[targetIndex].GetComponentInChildren<Text>().color = new Color(1, 1, 0, 0);
            targetIndex++;
        }

        else
        {
            Debug.Log("Wrong");
            RandomInputButtons();
            RandomTargetNumbers();
            targetIndex = 0;
        }

        if (targetIndex == 5)
        {
            gameMaster.winner = this.gameObject;
            StartCoroutine(gameMaster.ReloadScene());
            StartCoroutine(soundObject.PlayCutSound());
        }

        RandomInputButtons();
    }
}
