﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    [SerializeField] public Animator player1;
    [SerializeField] public Animator player2;
    [SerializeField] public Animator fade;
    [SerializeField] public GameObject winner;
    [SerializeField] public GameObject canvas;
    
    void Start()
    {
        StartCoroutine(FirstCoroutine(player1, player2));
    }

    void Update()
    {
        if (winner != null)
        {
            AnimatePlayers("Running");
            winner.GetComponent<Animator>().SetTrigger("Winning");
            canvas.SetActive(false);
        }
    }

    public IEnumerator FirstCoroutine(Animator player1, Animator player2)
    {
        yield return new WaitForSeconds(3);
        canvas.SetActive(true);
        AnimatePlayers("IdleWeapon");
    }

    public void AnimatePlayers(string trigger)
    {
        player1.SetTrigger(trigger);
        player2.SetTrigger(trigger);
    }

    public IEnumerator ReloadScene()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        yield return new WaitForSeconds(3);
        fade.SetTrigger("Fade");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(scene);
    }
}
