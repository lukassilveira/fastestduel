﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Clicker : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] Player player;

    public void OnPointerDown(PointerEventData eventData)
    {
        player.ComputeInput(gameObject);
    }
}
